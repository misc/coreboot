# Welcome to the Optiplex 760 docs!

### Status

#### Working

- VGA

- DisplayPort

- Sound (partial)

- Corebootfb Mode

- SeaBIOS

- GNU+Linux

#### Not Working

- Super I/O

- Fan control

- DisplayPort display at boot (it works when the kernel is loaded however)

#### Not tested yet

- GRUB

- Text Mode (should work fine)

- Blobless setup

- PCIE Graphics Card

- Windows (why would you want to install windows??????)

- BSD Systems (FreeBSD, NetBSD, OpenBSD, DragonFlyBSD, ...)

- GNU+Linux-libre

#### Bugs

- (Super I/O) The numbers 2, 3 and 4 are always blinking (Which means BIOS Corrupted when it's not)

- (Super I/O) Power Button is orange instead of green (Except when it is in suspend)

- (Fan control) Fan is always at max

#### Not implemented yet

- [X] Super I/O (SMSC SCH5524C)

- [X] Audio (needs reconfiguration to work properly)

- [X] Fan control

- ...

### Flashing

This motherboard uses a Winbond SOIC-8 Flash chip. Flashrom supports this chip

#### Internally

This motherboard has a flash chip that is rw protected, but by enabling service mode, you can bypass that restriction and flash normally with flashrom.

Flashrom supports this chip by the way!

(It is recommended to flash **only** the bios region, keeping other regions intact like ME, GbT and oher stuff, it's also recommended to include microcode updates.)

Don't forget to unplug your Optiplex after flashing, wait a few minutes and replug or else, it won't boot!

#### Externally

(info to be written soon but a Pomona SOIC-8 clip should work)
