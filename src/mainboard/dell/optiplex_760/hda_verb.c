	/* SPDX-License-Identifier: GPL-2.0-or-later */

#include <device/azalia_device.h>

const u32 cim_verb_data[] = {
	/* coreboot specific header */
	0x10ec0888,
	0x1025024c,	// Subsystem ID
	10,	// Number of entries

	/* Pin Widget Verb Table */

	AZALIA_PIN_CFG(0, 0x11, 0x02214040),
	AZALIA_PIN_CFG(0, 0x12, 0x01014010),
	AZALIA_PIN_CFG(0, 0x13, 0x991301f0),
	AZALIA_PIN_CFG(0, 0x14, 0x02a19020),
	AZALIA_PIN_CFG(0, 0x15, 0x01813030),
	AZALIA_PIN_CFG(0, 0x16, 0x413301f0),
	AZALIA_PIN_CFG(0, 0x17, 0x41a601f0),
	AZALIA_PIN_CFG(0, 0x1a, 0x41f301f0),
	AZALIA_PIN_CFG(0, 0x1b, 0x414501f0),
	AZALIA_PIN_CFG(0, 0x1c, 0x413301f0),


};

const u32 pc_beep_verbs[0] = {};

const u32 pc_beep_verbs_size = ARRAY_SIZE(pc_beep_verbs);
const u32 cim_verb_data_size = ARRAY_SIZE(cim_verb_data);
